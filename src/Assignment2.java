import java.util.ArrayList;
import java.util.Scanner;


public class Assignment2 {
	
	static ArrayList<Integer> number = new ArrayList<Integer>();
	static Scanner scan= new Scanner(System.in);
	
	public static void getNum(){

		System.out.println("Enter a number: ");
		number.add(scan.nextInt());
	}
	
	public static int getSum(){
		
		int sum = 0;
		for(int i=0; i<number.size();i++){
			
			sum+=number.get(i);
		}
		return sum;
	}
	
	public static int getAverage(){
		
		return getSum()/5;
	}
	
	public static int getMax(){
		int max= -900000;
		for (int i=0;i<number.size();i++){
			if (number.get(i) >max){
				max= number.get(i);
			}
		}
		return max;
	}
	
	public static int getMin(){
		int min= 900000;
		for (int i=0;i<number.size();i++){
			if (number.get(i) <min){
				min= number.get(i);
			}
		}
		return min;
	}
	
	public static void main(String[] args){
		getNum();
		getNum();
		getNum();
		getNum();
		getNum();
		System.out.println(getMin());
	}
}
